import Fibonnaci from "./fibonacci.js";

describe('Fibonacci Sequence', () => {
  describe('Units', () => {
    test('should number sequence 1 with result 1', () => {
      const numSequence = new Fibonnaci(1);
      expect(numSequence.calculateFibonacci()).toEqual(1);
    });

    test('should number sequence 3 with result 2', () => {
        const numSequence = new Fibonnaci(3);
        expect(numSequence.calculateFibonacci()).toEqual(2);
    });

    test('should number sequence 5 with result 5', () => {
        const numSequence = new Fibonnaci(5);
        expect(numSequence.calculateFibonacci()).toEqual(5);
    });
    
    test('should number sequence 7 with result 13', () => {
        const numSequence = new Fibonnaci(7);
        expect(numSequence.calculateFibonacci()).toEqual(13);
    });
    
    test('should number sequence 8 with result 21', () => {
        const numSequence = new Fibonnaci(8);
        expect(numSequence.calculateFibonacci()).toEqual(21);
    });

    test('should number sequence 10 with result 55', () => {
        const numSequence = new Fibonnaci(10);
        expect(numSequence.calculateFibonacci()).toEqual(55);
    });

    test('should number sequence 12 with result 144', () => {
        const numSequence = new Fibonnaci(12);
        expect(numSequence.calculateFibonacci()).toEqual(144);
    });

    test('should number sequence 14 with result 377', () => {
        const numSequence = new Fibonnaci(14);
        expect(numSequence.calculateFibonacci()).toEqual(377);
    });

    test('should number sequence 17 with result 1597', () => {
        const numSequence = new Fibonnaci(17);
        expect(numSequence.calculateFibonacci()).toEqual(1597);
    });
  });
});
