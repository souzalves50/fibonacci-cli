 class Fibonnaci {
    constructor(num){
      this.num = num;
    }
  
    calculateFibonacci() {
        let lastNumberFirst = 0;
        let lastNumberLast = 1;
        let fibonacci = 0;
        for (let i=1; i<=this.num; i++) {
            fibonacci = lastNumberFirst + lastNumberLast;
            lastNumberFirst = lastNumberLast;
            lastNumberLast = fibonacci;
            if (i==1) {
                lastNumberFirst = 0;
                lastNumberLast = 1;
            }
        }

      return(fibonacci);
    }
  }  

  module.exports = Fibonnaci;