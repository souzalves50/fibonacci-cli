#!/usr/bin/env node

const yargs = require('yargs');
const Fibonacci = require('./fibonacci.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <sequence>")
   .option("n", { alias: "sequence", describe: "Fibonacci Sequence", type: "int", demandOption: true })
   .argv;

  const f = new Fibonacci(options.n);
    
  console.log(f.calculateFibonacci());
}
